# CatAPI Test Project



## Description
This is a project that was given to me in order to validate some of my skills. 

Given the short amount of time I was given, and a vast space for creativity this project offers I decided to implement minimal required functionality and cover as 
much of the features that should be present in any given project.

The features that were particularly important to me were:

* Tests (I provided a single one to showcase its basic usage)
* Docstrings (Again, I provided just one, for that same reason)
* Documentation (You're reading it)

Other features that can be found in this implementation:

* argparse to handle console execution 

## How to run it

Just run

```bash
    python run.py cats <number of cats>
```

## What should you expect to get

* A .csv file with the specified number of cats data
* A .json file containing raw response data

## Original task

```
Create non-interactive code in Python (version not important) which gets random 100 cat pictures from the Cat API (free, https://thecatapi.com/), creates a JSON with url, breed, type, color for each cat (one file) then writes out to a CSV file. Provide source py file, result JSON and result CSV files.
```
