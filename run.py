import argparse

from cat_api.api_wrapper.cats import Cats
from cat_api.data_exporter.writers import csv_writer, json_writer


def run_script(args: argparse.Namespace) -> None:
    number_of_cats = args.number

    cats = Cats.get_some_cats(number_of_cats)
    dat = Cats.cats_to_writable_dict(cats)

    json_writer(cats)
    csv_writer(dat)


def parse_args():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    parser.set_defaults(func=lambda args: parser.print_help())

    parser_append = subparsers.add_parser(
        "cats", help="Specifies number of cats to be retrieved"
    )
    parser_append.add_argument("number", help="Number of cats")
    parser_append.set_defaults(func=run_script)

    return parser.parse_args()


def main():
    args = parse_args()
    args.func(args)


if __name__ == "__main__":
    main()
