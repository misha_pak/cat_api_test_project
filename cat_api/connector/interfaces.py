from abc import ABC, abstractmethod
from typing import Dict


class RequestParameters(ABC):
    host: str
    headers: Dict[str, str]

    @property
    @abstractmethod
    def request_url(self) -> str:
        raise NotImplementedError()
