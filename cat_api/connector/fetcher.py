from typing import Type, Dict, List

import requests

from cat_api.connector.interfaces import RequestParameters


def fetch(parameters: Type[RequestParameters]) -> List[Dict]:
    response = requests.get(
        url=parameters.request_url, headers=parameters.headers, timeout=10
    )

    response.raise_for_status()

    return response.json()
