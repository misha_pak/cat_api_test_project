import unittest

from cat_api.api_wrapper.cats import Cats


class ApiWrapperTests(unittest.TestCase):
    def test_get_cat_data_for_breeds(self):
        mock_breeds = [
            {"name": "Bengal"},
            {"name": "Cornish Rex"},
            {
                "name": "Manx",
                "alt_names": "Manks, Stubbin, Rumpy",
                "origin": "Isle of Man",
            },
        ]

        origins_data = Cats.get_cat_data(data=mock_breeds, field="origin")

        self.assertEquals(origins_data, "Isle of Man")

        names_data = Cats.get_cat_data(data=mock_breeds, field="name")

        self.assertEquals(names_data, "Bengal, Cornish Rex, Manx")
