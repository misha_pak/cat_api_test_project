from typing import Optional, Dict, List

from requests import HTTPError, ReadTimeout

from cat_api.api_wrapper.endpoints import SearchImagesParameters
from cat_api.configs import DEFAULT_NUMBER_OF_CATS
from cat_api.connector.fetcher import fetch
from cat_api.utils import list_to_str


class Cats:
    @classmethod
    def get_some_cats(
        cls, amount: Optional[int] = DEFAULT_NUMBER_OF_CATS, attempts: int = 3
    ) -> List[Dict]:
        """
         Gets a specified amount of cat entries that contain their image urls, breeds, and categories

        :param amount: Number of cats that needs to be retrieved, defaults to config value

        :return: List of cat data
        :raises Exception: Failed to get cats if max_attempts reached or host unreachable

        """
        if attempts < 1:
            raise Exception("Failed to get cats")

        search_parameters = SearchImagesParameters(limit=amount)

        try:
            cats = fetch(parameters=search_parameters)
        except HTTPError:
            raise Exception("Failed to get cats")
        except ReadTimeout:
            print("Please wait, trying to get some cats ...")
            return cls.get_some_cats(amount=amount, attempts=attempts - 1)

        return cats

    @classmethod
    def get_cat_data(cls, data: [], field: str) -> str:
        return list_to_str([breed[field] for breed in data if field in breed])

    @classmethod
    def cats_to_writable_dict(cls, cats: List[Dict]) -> List[Dict]:

        cats_writable = []

        for cat_data in cats:
            breeds = cat_data.get("breeds", [])
            categories = cat_data.get("categories", [])

            cat = {
                "url": cat_data["url"],
                "breeds": cls.get_cat_data(data=breeds, field="name"),
                "types": cls.get_cat_data(data=categories, field="name"),
                "alt_names": cls.get_cat_data(data=breeds, field="alt_names"),
                "origin": cls.get_cat_data(data=breeds, field="origin"),
                "weight_imperial": cls.get_cat_data(
                    data=breeds, field="weight_imperial"
                ),
            }
            cats_writable.append(cat)
        return cats_writable
