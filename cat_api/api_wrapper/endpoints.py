from abc import ABC, abstractmethod
from dataclasses import dataclass
from functools import reduce
from typing import Optional
from urllib.parse import urljoin

from cat_api import configs
from cat_api.connector.interfaces import RequestParameters


class CatAPIRequestParameters(RequestParameters, ABC):
    @property
    def host(self):
        return configs.CAT_API_HOST

    @property
    def headers(self):
        return {"x-api-key": configs.CAT_API_KEY}

    @abstractmethod
    def uri_parameter(self):
        raise NotImplementedError()


@dataclass
class SearchImagesParameters(CatAPIRequestParameters):
    limit: Optional[int]

    @property
    def uri_parameter(self) -> str:
        return "images/search"

    @property
    def request_url(self) -> str:
        url_parameters = [self.host, self.uri_parameter, self.request_parameters]

        return reduce(urljoin, url_parameters)

    @property
    def request_parameters(self) -> str:
        return f"?limit={self.limit}"
