import json
from typing import Dict

import pandas as pd

from cat_api.utils import get_current_timestamp


def csv_writer(data: Dict, file_name: str = f"cats_{get_current_timestamp()}.csv"):
    df = pd.DataFrame(data)
    df.to_csv(file_name, index=False)


def json_writer(data: Dict, file_name: str = f"cats_{get_current_timestamp()}.json"):
    json_object = json.dumps(data, indent=4)

    with open(file_name, "w") as json_file:
        json_file.write(json_object)
