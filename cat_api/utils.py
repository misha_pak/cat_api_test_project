from datetime import datetime
from typing import List


def get_current_timestamp() -> str:
    current_date = datetime.now()

    return current_date.strftime("%Y_%m_%dT%H_%M_%S")


def list_to_str(data: List) -> str:
    if len(data) == 0:
        return ""

    result = ""

    for item in data[:-1]:
        result += f"{item}, "

    result += data[-1]

    return result
